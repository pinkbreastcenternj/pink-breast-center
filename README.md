PINK Breast Center uses the latest digital equipment, including 3D imaging, to focus care on you with more comfort and less radiation. Whether you need breast imaging or general ultrasounds, a bone density scan, breast care, a breast biopsy, or a digital mammography screening, we’re here for you.

Address: 680 Broadway, #111, Paterson, NJ 07514, USA

Phone: 973-977-6662
